# NW.js 桌面软件打包
打包+一个文件+换图标

## 先下载代码

git clone https://gitee.com/yhss/NW.js-ZhuoMianRuanJianDaBao.git

----------------
## 1、打开app.zip

## 2、修改app.js 中的跳转连接，保存。zip弹出确定时，点确定

## 3、当前目录下按shift键,"在此处打开命令窗口"，执行以下
copy /b nw.exe+app.nw 1111.exe
## 4、点击“打包成一个文件.evb”，
使用“enigma virual box”打开。点击“process”打包。得到一个“1111_boxed.exe”

## 5、准备图标  
制作图标：http://www.bitbug.net/
或 下载图标： http://www.easyicon.net/
得到一个”xxx .icon”文件。大小为48X48

## 6、换图标
打开软件“Resource Hacker”，把“1111_boxed.exe”拉到左侧。
打开Icon Group>IDR_MAINFRAME>1033
按右键“Replace Resource”, 刚才准备的icon图标，
“CTRL+S”保存。OK！
